/*
 *  error_neuron.cpp
 *
 *  This file is part of NEST.
 *
 *  Copyright (C) 2004 The NEST Initiative
 *
 *  NEST is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  NEST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NEST.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "error_neuron.h"

// C++ includes:
#include <cmath> // in case we need isnan() // fabs
#include <cstdio>
#include <iomanip>
#include <iostream>
#include <limits>
#include <string>

// Includes from libnestutil:
#include "numerics.h"

// Includes from nestkernel:
#include "exceptions.h"
#include "kernel_manager.h"
#include "universal_data_logger_impl.h"

// Includes from sli:
#include "dict.h"
#include "dictutils.h"
#include "doubledatum.h"
#include "integerdatum.h"
#include "gauss_rate.h"

namespace nest
{

/* ----------------------------------------------------------------
 * Recordables map
 * ---------------------------------------------------------------- */

RecordablesMap< error_neuron >error_neuron::recordablesMap_;

template <>
void
RecordablesMap< error_neuron >::create()
{
  insert_( names::target_rate, &error_neuron::get_target_rate_ );
  // DEBUG II: the commented line below was used in the pattern generation task
  //insert_( names::learning_signal, &error_neuron::get_learning_signal_ );
  insert_( names::learning_signal, &error_neuron::get_last_ls_ );
  insert_( names::V_m, &error_neuron::get_V_m_ );
  insert_( names::len_eprop_hist, &error_neuron::get_eprop_history_len );
  insert_( names::len_ls_per_syn, &error_neuron::get_ls_per_syn_len );
}
/* ----------------------------------------------------------------
 * Default constructors defining default parameters and state
 * ---------------------------------------------------------------- */

nest::error_neuron::Parameters_::Parameters_()
  : tau_m_( 10.0 )                                  // ms
  , c_m_( 250.0 )                                   // pF
  , E_L_( -70.0 )                                   // mV
  , I_e_( 0.0 )                                     // pA
  , V_min_( -std::numeric_limits< double >::max() ) // relative E_L_-55.0-E_L_
  , t_start_ls_( 0.0 )                               // ms
  , t_start_filtering_( 0.0 ) // *Je* TODO: improve naming and documention of these variables.
  , ce_( 0.0 )                               // ms
  , ce_decay_( 0.0 )                               // ms
  , iteration_interval_( 0 )                               // ms
//  , learning_type_( 0 )
  , actor_id_( 0 )
  , batch_elem_interval_( 1 )                               // ms
{
}

nest::error_neuron::State_::State_()
  : target_rate_( 0.0 )
  , learning_signal_( 0.0 )
  , y0_( 0.0 )
  , y3_( 0.0 )
{
}

/* ----------------------------------------------------------------
 * Parameter and state extractions and manipulation functions
 * ---------------------------------------------------------------- */

void
nest::error_neuron::Parameters_::get(
  DictionaryDatum& d ) const
{
  def< double >( d, names::E_L, E_L_ ); // Resting potential
  def< double >( d, names::I_e, I_e_ );
  def< double >( d, names::V_min, V_min_ + E_L_ );
  def< double >( d, names::C_m, c_m_ );
  def< double >( d, names::tau_m, tau_m_ );
  def< double >( d, names::start, t_start_ls_ );
// *Je* TODO: improve naming and documention of these variables.
  def< double >( d, names::start_filtering, t_start_filtering_ );
  def< double >( d, names::ce, ce_ );
  def< double >( d, names::ce_decay, ce_decay_ );
  def< long >( d, names::iteration_interval, iteration_interval_ );
  def< long >( d, names::batch_elem_interval, batch_elem_interval_ );
//  def< long >( d, names::learning_type, learning_type_ );
  def< long >( d, names::actor_id, actor_id_ );
}

double
nest::error_neuron::Parameters_::set(
  const DictionaryDatum& d )
{
  const double ELold = E_L_;
  updateValue< double >( d, names::E_L, E_L_ );
  const double delta_EL = E_L_ - ELold;
  if ( updateValue< double >( d, names::V_min, V_min_ ) )
  {
    V_min_ -= E_L_;
  }

  updateValue< double >( d, names::I_e, I_e_ );
  updateValue< double >( d, names::C_m, c_m_ );
  updateValue< double >( d, names::tau_m, tau_m_ );
  updateValue< double >( d, names::start, t_start_ls_ );
  updateValue< double >( d, names::start_filtering, t_start_filtering_ );
// *Je* TODO: improve naming and documention of these variables.
  updateValue< double >( d, names::ce, ce_ );
  updateValue< double >( d, names::ce_decay, ce_decay_ );
  updateValue< long >( d, names::iteration_interval, iteration_interval_ );
  updateValue< long >( d, names::batch_elem_interval, batch_elem_interval_ );
//  updateValue< long >( d, names::learning_type, learning_type_ );
  updateValue< long >( d, names::actor_id, actor_id_ );

// *Je* TODO: introduce BadProperty handling of new varaibles.
  if ( c_m_ <= 0 )
  {
    throw BadProperty( "Capacitance must be >0." );
  }

  if ( tau_m_ <= 0 )
  {
    throw BadProperty( "Membrane time constant must be > 0." );
  }
  return delta_EL;
}

void
nest::error_neuron::State_::get(
  DictionaryDatum& d, const Parameters_& p ) const
{
  def< double >( d, names::target_rate, target_rate_); // target_rate
  def< double >( d, names::learning_signal, learning_signal_ );
  def< double >( d, names::V_m, y3_ + p.E_L_ ); // Membrane potential
}

void
nest::error_neuron::State_::set(
  const DictionaryDatum& d, const Parameters_& p, double delta_EL)
{
  updateValue< double >( d, names::target_rate, target_rate_ ); // target_rate
  updateValue< double >( d, names::learning_signal, learning_signal_ );

  if ( updateValue< double >( d, names::V_m, y3_ ) )
  {
    y3_ -= p.E_L_;
  }
  else
  {
    y3_ -= delta_EL;
  }
}


nest::error_neuron::Buffers_::Buffers_(
  error_neuron& n )
  : logger_( n )
{
}

nest::error_neuron::Buffers_::Buffers_(
  const Buffers_&,
  error_neuron& n )
  : logger_( n )
{
}

/* ----------------------------------------------------------------
 * Default and copy constructor for node
 * ---------------------------------------------------------------- */

nest::error_neuron::error_neuron()
  : EpropArchivingNode()
  , P_()
  , S_()
  , B_( *this )
{
  recordablesMap_.create();
}

nest::error_neuron::error_neuron(
  const error_neuron& n )
  : EpropArchivingNode( n )
  , P_( n.P_ )
  , S_( n.S_ )
  , B_( n.B_, *this )
{
}

/* ----------------------------------------------------------------
 * Node initialization functions
 * ---------------------------------------------------------------- */

void
nest::error_neuron::init_state_( const Node& proto )
{
  const error_neuron& pr = downcast< error_neuron >( proto );
  S_ = pr.S_;
}

void
nest::error_neuron::init_buffers_()
{
  B_.delayed_rates_.clear(); // includes resize

  B_.logger_.reset(); // includes resize
  B_.spikes_.clear();   // includes resize
  B_.currents_.clear(); // includes resize
  ArchivingNode::clear_history();
}

void
nest::error_neuron::calibrate()
{
  B_.logger_
    .init(); // ensures initialization in case mm connected after Simulate

  const double h = Time::get_resolution().get_ms();
  V_.P33_ = std::exp( -h / P_.tau_m_ );
  V_.P30_ = 1 / P_.c_m_ * ( 1 - V_.P33_ ) * P_.tau_m_;
  V_.step_start_ls_ = Time( Time::ms( std::max( P_.t_start_ls_, 0.0 ) ) ).get_steps();

  // *Je* TODO: improve naming this variable.
  V_.step_start_filtering_ = Time( Time::ms( std::max( P_.t_start_filtering_, 0.0 ) ) ).get_steps();
  // *Je* TODO: inform about this to Jonas/Agnes.
  RL_V_.action_space_dim_ = kernel().simulation_manager.get_action_space_dimension();
  RL_V_.trial_done_ = false;
}

/* ----------------------------------------------------------------
 * Update and event handling functions
 */

void
nest::error_neuron::update_( Time const& origin,
  const long from,
  const long to)
{
  assert(
    to >= 0 && ( delay ) from < kernel().connection_manager.get_min_delay() );
  assert( from < to );

  const size_t buffer_size = kernel().connection_manager.get_min_delay();

  size_t n_entries;
  // *Je* TODO: add better consistency in this kind of "learning type" ifs.
  if ( get_learning_type() != TD_RL )
  {
      n_entries = 2;
  }
  else
  {
      n_entries = 4;
  }
  // *Je* TODO: before it was called "readout_and_target_signasl", discuss with Jonas/Agnes.
  // allocate memory to store learning signal to be sent by learning signal events
  std::vector< double > learning_signal_buffer( n_entries*buffer_size, 0.0 );

  // allocate memory to store readout signal to be sent by rate events
  std::vector< double > readout_signal_buffer( buffer_size, 0.0 );

  // *Je* TODO: introduce documentation about this allocation.
  std::vector< double > to_critic_signal_buffer( 2*RL_V_.action_space_dim_*buffer_size, 0.0 );

  for ( long lag = from; lag < to; ++lag )
  {
    // DEBUG: added reset after each T to be compatible with tf code
    int t_mod_T = 0;
    int move = 0;
    double iteration=0;
    if (get_learning_type() != TD_RL)
    {
        t_mod_T = ( origin.get_steps() + lag - 3 ) % get_update_interval_steps();
    }
    else
    {
        // *Je* TODO: Why no passing n_trial_moves directly?
        // *Je* TODO: Maybe avoid "lag" for now?
        int n_trial_moves = P_.batch_elem_interval_ / get_update_interval_steps();
        t_mod_T = ( origin.get_steps() + lag - 0 ) % get_update_interval_steps();
        move = ( (origin.get_steps() + lag) / get_update_interval_steps() ) % n_trial_moves;
        // *Je* TODO: Change iteration -> episode
        iteration = floor( ( origin.get_steps() + lag  ) / P_.iteration_interval_ );
        
// *Je* TODO: eventually get rid of this commented lines.
//        if (P_.actor_id_ == 0)
//        {
//            std::cout \
//                << "{out} iteration: " << iteration\
//                << " move: " << move\
//                << " t_mod_t: " << t_mod_T\
//                << std::endl;
//        }
    }

    if ( get_learning_type() != TD_RL && t_mod_T == 0   )
    {
      S_.y3_ = 0.0;
      B_.spikes_.clear();   // includes resize
    }

    else if (get_learning_type() == TD_RL && move == 0)
    {
      S_.y3_ = 0.0;
      B_.spikes_.clear();   // includes resize
    }
    // DEBUG: introduced factor ( 1 - exp( -dt / tau_m ) ) for campatibility wit tf code
    S_.y3_ = V_.P30_ * ( S_.y0_ + P_.I_e_ ) + V_.P33_ * S_.y3_ + ( 1 - V_.P33_ ) * B_.spikes_.get_value( lag );
    S_.y3_ = ( S_.y3_ < P_.V_min_ ? P_.V_min_ : S_.y3_ );


    if (get_learning_type() == TD_RL) 
    {
      if ( t_mod_T == V_.step_start_ls_ - 6 )
      {
          S_.frozen_y3_ = S_.y3_;
      }
      else if ( t_mod_T > V_.step_start_ls_ - 6 )
      {
          S_.y3_ = S_.frozen_y3_;
      }
      else
      {
          S_.frozen_y3_ = 0.0;
      }
    }

    // compute the readout signal
    double readout_signal = S_.y3_ + P_.E_L_;
    // write exp( readout signal ) into the buffer which is used to send it to the other error neurons
    // in case of a regression task we don't need this and therefore set it to zero
    
    if (get_learning_type() == REGRESSION)
    {
        readout_signal_buffer[ lag ] = 0.0;
    }
    else 
    {
        readout_signal_buffer[ lag ]  = std::exp( readout_signal );
    }

    // *Je* TODO: Is this meaningful or it is just parasitic code from previous commits?
    if ( get_learning_type() == REWARD_BASED_CLASSIFICATION || get_learning_type() == TD_RL )
    {
        learning_signal_buffer[ n_entries*lag ] = origin.get_steps() + lag + 2;
    }
    else
    {
        // DEBUG: changed sign (see tf code)
        learning_signal_buffer[ n_entries*lag ] = origin.get_steps() + lag + 1;
    }

    // compute normalized learning signal from values stored in state_buffer_
    // which now contains the correct normalization because in the meantime
    // the other readout neurons have sent their membrane potential
    // the entries of the state_buffer_ are
    // 0: readout_signal, 1: target_signal, 2: normalization
    
    // *Je* TODO: improve naming and documention of these variables.
    // *Je* TODO: add Jonas original comments
    double pi_out = 0.0;
    double hot_encoded_action = 0.0;
    double ce_reg = 0.0;
    double normalized_learning_signal;
    double temporal_diff_error = 0.0;
    double entropy_reg_factor = 0.0;
    if ( t_mod_T >= V_.step_start_ls_ )
    {
      if ( get_learning_type() == REGRESSION )
      {
        normalized_learning_signal = V_.state_buffer_ [ READOUT ] - V_.state_buffer_ [ TARGET ];
      }
      else if (get_learning_type() == CLASSIFICATION)
      {
        normalized_learning_signal = std::exp( V_.state_buffer_ [ READOUT ] ) /
        V_.state_buffer_ [ NORM_FACTOR ] - V_.state_buffer_ [ TARGET ];
      }
      else if (get_learning_type() == REWARD_BASED_CLASSIFICATION)
      {
        ce_reg = P_.ce_* P_.ce_decay_ / (iteration + P_.ce_decay_);
        entropy_reg_factor = ce_reg * RL_V_.loc_prob_ * (std::log(RL_V_.loc_prob_) + RL_V_.entropy_);
        normalized_learning_signal = RL_V_.reward_minus_Vm_*RL_V_.normalized_ls_ + entropy_reg_factor; 

        // *Je* TODO: This 2 lines might be remove: check handle += policy
        RL_V_.reward_minus_Vm_ = 0.;
        RL_V_.entropy_ = 0.;
      }
      else if (get_learning_type() == TD_RL) 
      {
        if (RL_V_.trial_done_ == false)
        {
            normalized_learning_signal = RL_V_.normalized_ls_;
            temporal_diff_error = RL_V_.reward_ + RL_V_.temp_diff_err_;
            ce_reg = P_.ce_* P_.ce_decay_ / (iteration + P_.ce_decay_);
            entropy_reg_factor = ce_reg * RL_V_.loc_prob_ * (std::log(RL_V_.loc_prob_) + RL_V_.entropy_);
        }

// *Je* TODO: eventually get rid of this commented lines.
//        if (P_.actor_id_ == 0)
//        {  
//            std::cout << "(F)[NEST][ls] t: " << origin.get_steps() + lag << " ("<< t_mod_T\
//                    << ") reward: " << RL_V_.reward_\
//                    <<" iteration: " << iteration\
//                    <<" ce_reg: " << ce_reg\
//                    << " entr_reg_fac: "<< entropy_reg_factor\
//                    <<" ls: " << normalized_learning_signal\
//                    << " temp_diff_err: " << temporal_diff_error \
//                    << std::endl;
//        } // LSDEBUG!

        // *Je* TODO: This 2 lines might be remove: check handle += policy
        RL_V_.temp_diff_err_ = 0.; 
        RL_V_.entropy_ = 0.;
      }
    }
    else
    {
      normalized_learning_signal = 0.0;
    }


    int extra_advance = 0;
    if (get_learning_type() == TD_RL)
    {
        extra_advance = 3;
    }

    if ( t_mod_T >= V_.step_start_ls_ - 1 - extra_advance )
    {
      if ( get_learning_type() == REGRESSION)
      {
        V_.state_buffer_ [ READOUT ] = readout_signal;
        V_.state_buffer_ [ TARGET ] = S_.target_rate_;
        V_.state_buffer_ [ NORM_FACTOR ] = 1.0;
      }
      else if ( get_learning_type() == CLASSIFICATION )
      {
        V_.state_buffer_ [ READOUT ] = readout_signal;
        V_.state_buffer_ [ TARGET ] = S_.target_rate_;
        V_.state_buffer_ [ NORM_FACTOR ] = std::exp( readout_signal );
      }
    }
    else
    {
      // *Je* TODO: add better consistency in this kind of "learning type" ifs.
      if ( get_learning_type() != REWARD_BASED_CLASSIFICATION && get_learning_type() != TD_RL)
      {
        V_.state_buffer_ [ READOUT ] = 0.0;
        V_.state_buffer_ [ TARGET ] = 0.0;
        V_.state_buffer_ [ NORM_FACTOR ] = 1.0;
      }
      if ( get_learning_type() == TD_RL)
      {
         RL_V_.action_taken_here_ = false;
      }
    }
   
    if ( get_learning_type() == REWARD_BASED_CLASSIFICATION || get_learning_type() == TD_RL )
    {
      if ( t_mod_T == V_.step_start_ls_ - 2 - extra_advance )
      { 
        pi_out = std::exp( V_.state_buffer_ [ READOUT ] ) / V_.state_buffer_ [ NORM_FACTOR ];
        RL_V_.mean_output_signal_ += pi_out;
        RL_V_.loc_prob_ = RL_V_.mean_output_signal_ / (V_.step_start_ls_ - V_.step_start_filtering_ + 1);

// *Je* TODO: eventually get rid of this commented lines.
//        if (P_.actor_id_ == 0)
//        { 
//            std::cout << "\n(A)[NEST][pi] t: " << origin.get_steps() + lag << " ("<< t_mod_T\
//            << ") ent_reg_fac: " << entropy_reg_factor << " t_diff_err: " << temporal_diff_error << std::endl;
//        }

        RL_V_.normalized_ls_ = RL_V_.loc_prob_; 
        RL_V_.mean_output_signal_ = 0.0;
        V_.state_buffer_ [ TARGET ] = S_.target_rate_;  // *Je* TODO: is this line parasitic?
        if (get_learning_type() == TD_RL)
        {
            RL_V_.action_taken_here_ = false;
        }
      }
      // *Je* TODO: can this if {} be remove?
      if ( t_mod_T < V_.step_start_ls_ - 2 - extra_advance)
      {
        RL_V_.reward_minus_Vm_ = 0.0;
        RL_V_.normalized_ls_ = 0.0;
      }

      if ( t_mod_T >= V_.step_start_filtering_ - 2 - extra_advance)
      {
        pi_out = std::exp( V_.state_buffer_ [ READOUT ] ) / V_.state_buffer_ [ NORM_FACTOR ];
        RL_V_.mean_output_signal_ += pi_out;
      }
      else
      {
      // *Je* TODO: can this else {} be remove?
         RL_V_.mean_output_signal_ = 0.0; 
      }

      if ( t_mod_T >= V_.step_start_filtering_ - 3 - extra_advance)
      {
        V_.state_buffer_ [ READOUT ] = readout_signal;
        V_.state_buffer_ [ NORM_FACTOR ] = std::exp( readout_signal );
      }
      else
      {
        V_.state_buffer_ [ READOUT ] = 0.0;
        V_.state_buffer_ [ TARGET ] = 0.0;
        V_.state_buffer_ [ NORM_FACTOR ] = 1.0;
      }
    }


    // *Je* TODO: add documentation, remove "lag"
    if (get_learning_type() == REWARD_BASED_CLASSIFICATION )
    {
        for ( long id = 0; id < RL_V_.action_space_dim_; ++id )
        {
            if ( P_.actor_id_ == id )
            { 
                to_critic_signal_buffer[ lag + id] = RL_V_.loc_prob_;
                to_critic_signal_buffer[ lag + RL_V_.action_space_dim_ + id ] = V_.state_buffer_[ TARGET ];
            }
        }
    }
    else if ( get_learning_type() == TD_RL )
    {
        for ( long id = 0; id < RL_V_.action_space_dim_; ++id )
        {
            if ( P_.actor_id_ == id )
                to_critic_signal_buffer[ lag + id] = RL_V_.loc_prob_;
        }
    }
    else
    { 
        to_critic_signal_buffer[ lag ]  = 0.0;
    }

    // write normalized learning signal into history. Use the previous time step:
    // origin.get_steps() + lag (without + 1) because of the buffering in
    // readout_signal_buffer
    // previously, the function write_readout_history was used for this purpose
    Time const& t_norm_ls = Time::step( origin.get_steps() + lag );
    const double t_norm_ls_ms = t_norm_ls.get_ms();
    // *Je* TODO: add better consistency in this kind of "learning type" ifs.
    if (get_learning_type() == TD_RL)
    {  
        eprop_history_.push_back( histentry_eprop( t_norm_ls_ms-5.0*Time::get_resolution().get_ms(),
              0.0, normalized_learning_signal, temporal_diff_error, entropy_reg_factor, 0 ) );
    }
    else
    {
    // *Je* TODO: original class histentry eprop was generalized, maybe look
    // for overloading the constructor.
        eprop_history_.push_back( histentry_eprop( t_norm_ls_ms,
          0.0, normalized_learning_signal, 0.0, 0.0, 0 ) ); // Generalized to include TD error
    }

    // store the normalized learning signal in the buffer which is send to
    // the recurrent neurons via the learning signal connection
    learning_signal_buffer[ n_entries*lag + 1 ] = normalized_learning_signal;
    // *Je* TODO: add better consistency in this kind of "learning type" ifs.
    if (get_learning_type() == TD_RL)
    { 
        learning_signal_buffer[ n_entries*lag + 2 ] = temporal_diff_error;
        learning_signal_buffer[ n_entries*lag + 3 ] = entropy_reg_factor;
    }
    else
    {
        learning_signal_buffer[ n_entries*lag + 2 ] = 0.;
        learning_signal_buffer[ n_entries*lag + 3 ] = 0.;
    }

    S_.y0_ = B_.currents_.get_value( lag ); // set new input current
    S_.target_rate_ =  B_.delayed_rates_.get_value( lag );

    B_.logger_.record_data( origin.get_steps() + lag );
  }

  // send learning signal
  // TODO: it would be much more efficient to send this in larger batches
  LearningSignalConnectionEvent drve;
  drve.set_coeffarray( learning_signal_buffer );
  kernel().event_delivery_manager.send_secondary( *this, drve );
  // time one time step larger than t_mod_T_final because the readout has to
  // be sent one time step in advance so that the normalization can be computed
  // and the learning signal is ready as soon as the recall starts.
  // *Je* TODO: add better consistency in this kind of "learning type" ifs.
  if ( get_learning_type() != REGRESSION )
  {
    // send readout signal only if this is a classification task
    // rate connection to connect to other readout neurons
    DelayedRateConnectionEvent readout_event;
    readout_event.set_coeffarray( readout_signal_buffer );
    kernel().event_delivery_manager.send_secondary( *this, readout_event );
  }
  if ( get_learning_type() == REWARD_BASED_CLASSIFICATION || get_learning_type() == TD_RL )
  {
    CritiqueRequestConnectionEvent to_critic_event;
    to_critic_event.set_coeffarray( to_critic_signal_buffer );
    kernel().event_delivery_manager.send_secondary( *this, to_critic_event );
  }
  if ( get_learning_type() == TD_RL && RL_V_.action_taken_here_ == true )
  {

    int T_used = ( origin.get_steps() ) % get_update_interval_steps();
    int T_real =  origin.get_steps() ;
// *Je* TODO: eventually get rid of this commented lines.
//    std::cout << "(C)[NEST][spk] t: " << T_real << " (" << T_used << ")  Actor: " << P_.actor_id_\ 
//           << " temp_diff_err: " <<  RL_V_.temp_diff_err_  << std::endl;

    SpikeEvent se;
    kernel().event_delivery_manager.send( *this, se, 0 );
    RL_V_.action_taken_here_ = false;
  }


  return;
}

bool
nest::error_neuron::is_eprop_readout()
{
  return true;
}

bool
nest::error_neuron::is_eprop_critic()
{
  return false;
}


void
nest::error_neuron::handle(
  DelayedRateConnectionEvent& e )
{
  assert( 0 <= e.get_rport() && e.get_rport() < 3 );
  const double weight = e.get_weight();
  const long delay = e.get_delay_steps();
  size_t i = 0;
  std::vector< unsigned int >::iterator it = e.begin();
  if ( e.get_rport() == READOUT_SIG - MIN_RATE_RECEPTOR )
  {
    // handle port for readout signal
    // The call to get_coeffvalue( it ) in this loop also advances the iterator it
    while ( it != e.end() )
    {
      double readout_signal = e.get_coeffvalue( it );
      V_.state_buffer_ [ NORM_FACTOR ] += readout_signal;
      ++i;
    }
  }
  else  if ( e.get_rport() == TARGET_SIG - MIN_RATE_RECEPTOR )
  {
    // handle port for target signal
    while ( it != e.end() )
    {
      // The call to get_coeffvalue( it ) in this loop also advances the iterator it
      B_.delayed_rates_.add_value(delay + i, weight * 1. * e.get_coeffvalue( it ) ) ;
      ++i;
    }
  }
}

void
nest::error_neuron::handle(
  CritiqueFeedbackConnectionEvent& e )
{
  assert( 0 <= e.get_rport() && e.get_rport() < 1 );
  size_t i = 0;

  std::vector< unsigned int >::iterator it = e.begin();

  const Time stamp = e.get_stamp();
  double t_ms = stamp.get_ms();

  while ( it != e.end() )
  {
    double RV = e.get_coeffvalue( it );
    RL_V_.reward_minus_Vm_ += RV;
    double entropy = e.get_coeffvalue( it );
    RL_V_.entropy_ += entropy;
    double temp_diff_err = e.get_coeffvalue( it );
    RL_V_.temp_diff_err_ += temp_diff_err;

//    if (P_.actor_id_ == 0)
//        std::cout << "Critique, t: " << t_ms << " TD: " << RL_V_.temp_diff_err_ << std::endl;

    for ( long id = 0; id < RL_V_.action_space_dim_; ++id )
    {
        double hot_encoded_id = e.get_coeffvalue( it );
        if (P_.actor_id_ == id)
        {
            RL_V_.normalized_ls_ -= hot_encoded_id;
            if (get_learning_type() == TD_RL && hot_encoded_id > 0.0)
                RL_V_.action_taken_here_ = true;
        }
    }
    ++i;
  }
}

void
nest::error_neuron::handle(
  InstantaneousRateConnectionEvent& e )
{
  assert( 0 <= e.get_rport() && e.get_rport() < 2 );
  size_t i = 0;
  std::vector< unsigned int >::iterator it = e.begin();

  const Time stamp = e.get_stamp();
  double t_ms = stamp.get_ms();

  if ( e.get_rport() == 0 )
  {
      while ( it != e.end() )
      {
        double reward = e.get_coeffvalue( it );
        RL_V_.reward_ = reward;

    //    if (P_.actor_id_ == 0)
    //    { 
    //        std::cout << ">> << actor id: " << P_.actor_id_ << " time: " << t_ms << " reward: " << reward \
    //            << " TD: "<< RL_V_.temp_diff_err_ << std::endl; 
    //    }
    
        ++i;
      }
  }
  else if ( e.get_rport() == 1 )
  {
      while ( it != e.end() )
      {
        double trial_done_sig = e.get_coeffvalue( it );
        if (trial_done_sig > 0.5)
        { 
            RL_V_.trial_done_ = true; 

        }
        else
        {
            RL_V_.trial_done_ = false;
        }
    
//        if (P_.actor_id_ == 0 && RL_V_.trial_done_ == true)
//        { 
//            std::cout << "** time: " << t_ms << " Done" << std::endl; 
//        }
        ++i;
      }
  }
}


void
nest::error_neuron::handle( SpikeEvent& e )
{
  assert( e.get_delay_steps() > 0 );

  // EX: We must compute the arrival time of the incoming spike
  //     explicity, since it depends on delay and offset within
  //     the update cycle.  The way it is done here works, but
  //     is clumsy and should be improved.
  B_.spikes_.add_value(
    e.get_rel_delivery_steps( kernel().simulation_manager.get_slice_origin() ),
    e.get_weight() * e.get_multiplicity() );
}

void
nest::error_neuron::handle( CurrentEvent& e )
{
  assert( e.get_delay_steps() > 0 );

  const double c = e.get_current();
  const double w = e.get_weight();

  // add weighted current; HEP 2002-10-04
  B_.currents_.add_value(
    e.get_rel_delivery_steps( kernel().simulation_manager.get_slice_origin() ),
    w * c );
}

void
nest::error_neuron::handle( DataLoggingRequest& e )
{
  B_.logger_.handle( e );
}

} // namespace
